import matplotlib.pyplot as plt
import csv

keys = ("ks_name", "ks_difficulty", "use_rule_solve", "rule_solve_end_condition", "rule_solve_max_rdv",
        "rule_solve_timeout", "rule_solve_max_blocks", "rule_solve_max_cells",
        "next_strategy", "search_timeout", "cell_order_option", "minizinc_gen_pseudos", "minizinc_max_blocks",
        "minizinc_max_cells", "rule_solve_status", "rule_solver_rdv", "rule_solve_time", "search_solve_status",
        "search_solve_time", "minizinc_solve_status", "minizinc_solve_time", "total_solve_time")

records = []
with open("exp_2a_results.csv") as f:

    r = csv.reader(f)

    next(r)
    next(r)

    for row in r:
        data = row
        for i in range(0, len(data)):
            data[i] = data[i].strip()

        data = tuple(data)
        data_dict = dict(zip(keys, data))

        for k in ["use_rule_solve", "rule_solve_status", "search_solve_status", "minizinc_solve_status", "minizinc_gen_pseudos"]:
            if data_dict[k] == "True":
                data_dict[k] = True
            elif data_dict[k] == "False":
                data_dict[k] = False

        for k in ["rule_solve_max_rdv", "rule_solve_timeout", "rule_solve_max_blocks", "rule_solve_max_cells",
                  "search_timeout", "rule_solver_rdv", "minizinc_max_blocks", "minizinc_max_cells",
                  "rule_solve_time", "search_solve_time", "minizinc_solve_time", "total_solve_time"]:
            if data_dict[k] != "n/a":
                data_dict[k] = int(data_dict[k])

        records.append(data_dict)

x = []
y_bt = []
y_bj = []
y_fc = []

for record in records:

    if record["rule_solver_rdv"] < 700:
        if record["search_solve_status"] is True:
            if record["next_strategy"] == "backtrack":
                y_bt.append(record["search_solve_time"])
            elif record["next_strategy"] == "backjump":
                y_bj.append(record["search_solve_time"])
            else:
                x.append(record["rule_solver_rdv"])
                y_fc.append(record["search_solve_time"])

plt.xlabel("Remaining domain values")
plt.ylabel("Search solve time (ms)")

plt.plot(x, y_bt, label="backtrack")
plt.plot(x, y_bj, label="backjump")
plt.plot(x, y_fc, label="forward checking")

plt.legend()
plt.show()
