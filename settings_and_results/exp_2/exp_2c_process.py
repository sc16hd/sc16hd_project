import matplotlib.pyplot as plt
import numpy as np
import csv

keys = ("ks_name", "ks_difficulty", "use_rule_solve", "rule_solve_end_condition", "rule_solve_max_rdv",
        "rule_solve_timeout", "rule_solve_max_blocks", "rule_solve_max_cells",
        "next_strategy", "search_timeout", "cell_order_option", "minizinc_gen_pseudos", "minizinc_max_blocks",
        "minizinc_max_cells", "rule_solve_status", "rule_solver_rdv", "rule_solve_time", "search_solve_status",
        "search_solve_time", "minizinc_solve_status", "minizinc_solve_time", "total_solve_time")

records = []
with open("exp_2c_results.csv") as f:

    r = csv.reader(f)

    next(r)
    next(r)

    for row in r:
        data = row
        for i in range(0, len(data)):
            data[i] = data[i].strip()

        data = tuple(data)
        data_dict = dict(zip(keys, data))

        for k in ["use_rule_solve", "rule_solve_status", "search_solve_status", "minizinc_solve_status", "minizinc_gen_pseudos"]:
            if data_dict[k] == "True":
                data_dict[k] = True
            elif data_dict[k] == "False":
                data_dict[k] = False

        for k in ["rule_solve_max_rdv", "rule_solve_timeout", "rule_solve_max_blocks", "rule_solve_max_cells",
                  "search_timeout", "rule_solver_rdv", "minizinc_max_blocks", "minizinc_max_cells",
                  "rule_solve_time", "search_solve_time", "minizinc_solve_time", "total_solve_time"]:
            if data_dict[k] != "n/a":
                data_dict[k] = int(data_dict[k])

        records.append(data_dict)

for num in [150, 250, 350]:

    ks_records = dict()
    for record in records:
        if record["ks_name"] not in ks_records.keys():
            ks_records[record["ks_name"]] = []
        if record["rule_solve_max_rdv"] == num and record["search_solve_status"] is True:
            ks_records[record["ks_name"]].append(record)

    bt_count = 0
    bj_count = 0
    fc_count = 0
    for ks_name, ks_records in ks_records.items():
        if len(ks_records) != 0:
            lowest_time_r = None
            for record in ks_records:
                if lowest_time_r is None:
                    lowest_time_r = record
                elif record["search_solve_time"] < lowest_time_r["search_solve_time"]:
                    lowest_time_r = record
            if lowest_time_r["next_strategy"] == "backtrack":
                bt_count += 1
            elif lowest_time_r["next_strategy"] == "backjump":
                bj_count += 1
            elif lowest_time_r["next_strategy"] == "fc":
                fc_count += 1

    print(str(num) + " RDVs: ")
    print("    backtrack: " + str(bt_count))
    print("    backjump: " + str(bj_count))
    print("    forward checking: " + str(fc_count) + "\n")
