rdvs = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230,
        240, 250, 260, 270, 280, 290, 300, 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420, 430]

multi_config = {
    "output_option": "one_file",
    "ks_name": ['dks_8229'],
    "use_rule_solve": [True],
    "rule_solve_end_condition": ["rdv"],
    "rule_solve_min_rdv": rdvs,
    "rule_solve_timeout": [0],
    "rule_solve_max_blocks": [4],
    "rule_solve_max_cells": [4],
    "next_strategy": ["backtrack", "backjump", "fc"],
    "search_timeout": [0],
    "cell_order_option": ["d"],
    "minizinc_gen_pseudos": [False],
    "minizinc_max_blocks": [4],
    "minizinc_max_cells": [4]
}
