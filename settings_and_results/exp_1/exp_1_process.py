import matplotlib.pyplot as plt
import numpy as np

import csv


keys = ("ks_name", "ks_difficulty", "use_rule_solve", "rule_solve_end_condition", "rule_solve_max_rdv",
        "rule_solve_timeout", "rule_solve_max_blocks", "rule_solve_max_cells",
        "next_strategy", "search_timeout", "cell_order_option", "minizinc_gen_pseudos", "minizinc_max_blocks",
        "minizinc_max_cells", "rule_solve_status", "rule_solver_remaining_domain_values", "rule_solve_time",
        "search_solve_status", "search_solve_time", "minizinc_solve_status", "minizinc_solve_time",
        "gecode_solve_time", "total_solve_time")

records = []

with open("exp_1_results.csv") as f:
    r = csv.reader(f)

    next(r)
    next(r)

    for row in r:
        data = row
        for i in range(0, len(data)):
            data[i] = data[i].strip()

        data = tuple(data)
        data_dict = dict(zip(keys, data))

        for k in ["use_rule_solve", "rule_solve_status", "search_solve_status", "minizinc_solve_status",
                  "minizinc_gen_pseudos"]:
            if data_dict[k] == "True":
                data_dict[k] = True
            elif data_dict[k] == "False":
                data_dict[k] = False

        for k in ["ks_difficulty", "rule_solve_max_rdv", "rule_solve_timeout", "rule_solve_max_blocks",
                  "rule_solve_max_cells", "search_timeout", "rule_solver_remaining_domain_values",
                  "minizinc_max_blocks", "minizinc_max_cells", "rule_solve_time", "search_solve_time",
                  "minizinc_solve_time", "gecode_solve_time", "total_solve_time"]:
            if data_dict[k] != "n/a":
                data_dict[k] = int(data_dict[k])

        records.append(data_dict)

separate_records = dict()
for record in records:
    if record["ks_name"] not in separate_records.keys():
        separate_records[record["ks_name"]] = []
    separate_records[record["ks_name"]].append(record)

rs_better = 0
solved_by_rs = 0
for ks, records in separate_records.items():
    lowest_time_record_rs_t = None
    for record in records:
        if record["rule_solve_status"] is True:
            if lowest_time_record_rs_t is None:
                lowest_time_record_rs_t = record
            elif record["total_solve_time"] < lowest_time_record_rs_t["total_solve_time"]:
                lowest_time_record_rs_t = record
        elif record["use_rule_solve"] is False:
            mz_record = record
    if lowest_time_record_rs_t is not None:
        solved_by_rs += 1
        if lowest_time_record_rs_t["total_solve_time"] < (mz_record["gecode_solve_time"]):
            rs_better += 1

print("Puzzles type A config could solve: " + str(solved_by_rs))
print("Puzzles type A config was the best: " + str(rs_better))
