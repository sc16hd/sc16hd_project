import csv

keys = ("ks_name", "ks_difficulty", "use_rule_solve", "rule_solve_end_condition", "rule_solve_max_rdv",
        "rule_solve_timeout", "rule_solve_max_blocks", "rule_solve_max_cells",
        "next_strategy", "search_timeout", "cell_order_option", "minizinc_gen_pseudos", "minizinc_max_blocks",
        "minizinc_max_cells", "rule_solve_status", "rule_solver_rdv", "rule_solve_time", "search_solve_status",
        "search_solve_time", "minizinc_solve_status", "minizinc_solve_time", "total_solve_time")

records = []

with open("exp_3_results.csv") as f:
    r = csv.reader(f)

    next(r)
    next(r)

    for row in r:
        data = row
        for i in range(0, len(data)):
            data[i] = data[i].strip()

        data = tuple(data)
        data_dict = dict(zip(keys, data))

        for k in ["use_rule_solve", "rule_solve_status", "search_solve_status", "minizinc_solve_status",
                  "minizinc_gen_pseudos"]:
            if data_dict[k] == "True":
                data_dict[k] = True
            elif data_dict[k] == "False":
                data_dict[k] = False

        for k in ["ks_difficulty", "rule_solve_max_rdv", "rule_solve_timeout", "rule_solve_max_blocks",
                  "rule_solve_max_cells", "search_timeout", "rule_solver_rdv",
                  "minizinc_max_blocks", "minizinc_max_cells", "rule_solve_time", "search_solve_time",
                  "minizinc_solve_time", "total_solve_time"]:
            if data_dict[k] != "n/a":
                data_dict[k] = int(data_dict[k])

        records.append(data_dict)

separate_records = dict()
for record in records:
    if record["ks_name"] not in separate_records.keys():
        separate_records[record["ks_name"]] = []
    separate_records[record["ks_name"]].append(record)

lowest_records = []
mzn_records = []
for ks, records in separate_records.items():
    lowest_time_record = None
    for record in records:
        if record["minizinc_gen_pseudos"] is False:
            mzn_records.append(record)
        if lowest_time_record is None:
            lowest_time_record = record
        elif record["total_solve_time"] <= lowest_time_record["total_solve_time"]:
            lowest_time_record = record
    lowest_records.append(lowest_time_record)

count_c_better_than_d = 0
count_c_better_by_0_50 = 0
highest_perc_diff = 0
highest_perc_diff_rec = None

block_cell_count = dict()

for record in lowest_records:
    if record["minizinc_gen_pseudos"]:
        count_c_better_than_d += 1
        for mzn_record in mzn_records:
            if mzn_record["ks_name"] == record["ks_name"]:
                mzn_record_t = mzn_record["total_solve_time"]
                record_t = record["total_solve_time"]

                if (record["minizinc_max_blocks"], record["minizinc_max_cells"]) not in block_cell_count.keys():
                    block_cell_count[(record["minizinc_max_blocks"], record["minizinc_max_cells"])] = 0
                block_cell_count[(record["minizinc_max_blocks"], record["minizinc_max_cells"])] += 1

                if mzn_record_t >= record_t:

                    perc_diff = ((record_t - mzn_record_t) / (mzn_record_t)) * -100

                    if perc_diff > highest_perc_diff:
                        highest_perc_diff = perc_diff
                        highest_perc_diff_rec = record

                    if 0.1 < perc_diff <= 50:
                        count_c_better_by_0_50 += 1

                break

print("Type C config better: " + str(count_c_better_than_d))
print("Type C configs better by 0.1-50%: " + str(count_c_better_by_0_50 ))
print("Highest decrease in run time " + str(round(highest_perc_diff, 1)) + "% for puzzle " +
      highest_perc_diff_rec["ks_name"])
