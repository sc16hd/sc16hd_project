# Killer Sudoku Solver

## Installation instructions

### Required software

 * Python 3.6 or above
   * Follow instructions found here:  https://docs.python.org/3/using/unix.html#getting-and-installing-the-latest-version-of-python

 * Firefox
   * Follow instructions found here: https://support.mozilla.org/en-US/products/firefox/install-and-update-firefox

 * geckodriver
   * Download the version of geckodriver relevant to your system here: https://github.com/mozilla/geckodriver/releases/tag/v0.26.0
   * Extract the downloaded .tar.gz file by running:

     `tar -xvzf <downloaded tar.gz file>`
   * An executable 'geckodriver' will be extracted. The path of the executable must be added to PATH.  To do this, run the following command replacing  <path/to/directory/containing/geckodriver/executable> with the absolute path to the directory containing the geckodriver  executable:

	 `export PATH=$PATH:<path/to/directory/containing/geckodriver/executable>`

* MiniZinc
  * Follow instructions found here: https://www.minizinc.org/doc-2.4.3/en/installation.html

### Installing required Python packages

Execute the following commands inserting the paths where necessary to create a virtual environment and install the required Python packages:

1. ` python3 -m venv </path/to/venv> `
2. `source </path/to/venv>/bin/activate`  
3. `pip install -e </path/to/sc16hd_project/Killer_Sudoku_Solver/>`  
4. `pip install minizinc`  
5. `pip install numpy`  
6. `pip install selenium`  
7. `pip install matplotlib`

## Running the solver

Execute the following command to view the running options of the solver:
`python Killer_Sudoku_Solver/bin/ks_solver.py --help`
