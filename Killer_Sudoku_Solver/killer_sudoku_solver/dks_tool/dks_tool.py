from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import numpy as np
import time
import os


def main(puzzle_ids):

    options = Options()
    options.headless = True

    driver = webdriver.Firefox(options=options, service_log_path="/dev/null")

    abs_path = os.path.abspath(__file__)
    dir_name = os.path.dirname(abs_path)
    os.chdir(dir_name)

    # puzzle_ids = np.random.randint(1, 20000, 50)
    # puzzle_ids = [str(x) for x in puzzle_ids]
    # puzzle_ids = ["15434"]

    print("Puzzles with IDs " + str(puzzle_ids) + " will be downloaded.")
    print("Note that puzzles that are 'greater-than' Killer Sudokus will not be downloaded. \n")

    created_puzzle_files = []
    for puzzle_id in puzzle_ids:
        print("Downloading puzzle " + puzzle_id + "...")

        try:
            driver.get("https://www.dailykillersudoku.com/puzzle/" + puzzle_id)
            ks_string = driver.execute_script(open("dks_tool_script.js").read())

            if len(ks_string) != 0:
                with open("../data/ks_puzzles/dks_" + puzzle_id + ".csv", "w") as f:
                    for cage in ks_string:
                        f.write(cage + "\n")
                created_puzzle_files.append("dks_" + puzzle_id + ".csv")
        except:
            pass

        time.sleep(5)

    print("\nNew puzzle files created in Killer_Sudoku_Solver/killer_sudoku_solver/data/ks_puzzles: ")
    print(created_puzzle_files)

    driver.quit()
