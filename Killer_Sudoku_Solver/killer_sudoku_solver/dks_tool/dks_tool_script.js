var difficulty = window.DKS.puzzle._json.difficulty;
var puzzle_type = window.DKS.puzzle._json.puzzle_type;

if (puzzle_type != 1)
{
    return [];
}

var all_cage_strings = [];
var cages = window.DKS.puzzle.board._cages;

all_cage_strings.push(difficulty.toString());

for (i = 0; i < cages.length; i++)
{
    var cage_string = "\"";
    var cage = cages[i];
    var cage_cells = cage.cells;

    for (j = 0; j < cage_cells.length; j++)
    {
        var cell = cage_cells[j];
        cage_string = cage_string.concat(cell.row.toString());
        cage_string = cage_string.concat("|");
        cage_string = cage_string.concat(cell.column.toString());

        if (j != (cage_cells.length - 1))
        {
            cage_string = cage_string.concat(",");
        }
    }

    cage_string = cage_string.concat("\",");
    cage_string = cage_string.concat(cage.sum.toString());
    all_cage_strings.push(cage_string);
}

return all_cage_strings;
