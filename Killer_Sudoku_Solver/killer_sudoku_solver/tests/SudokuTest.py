import unittest

from killer_sudoku_solver.core.SolvingRun import SolvingRun


class SudokuTest(unittest.TestCase):

    def setUp(self):

        self.ks1 = SolvingRun.load_ks("dks_310")

    def tearDown(self):

        del self.ks1

    def test__elim_using_row__7_and_8_in_row__7_and_8_removed_from_pencil_values(self):

        self.ks1.get_cell(0, 3).set_pencil_values({7}, rule_solve=True)
        self.ks1.get_cell(0, 4).set_pencil_values({8}, rule_solve=True)
        self.ks1.get_cell(0, 7).set_pencil_values({5, 6, 7, 8}, rule_solve=True)

        self.ks1.elim_house("row", 0)

        self.assertTrue(self.ks1.get_cell(0, 3).get_complete())
        self.assertTrue(self.ks1.get_cell(0, 4).get_complete())
        self.assertEqual(self.ks1.get_cell(0, 7).get_pencil_values(), {5, 6})
        self.assertEqual(self.ks1.get_cell(0, 0).get_pencil_values(), {1, 2, 3, 4, 5, 6, 9})
        self.assertEqual(self.ks1.get_cell(0, 8).get_pencil_values(), {1, 2, 3, 4, 5, 6, 9})

    def test__elim_using_box__only_pencil_values(self):

        self.ks1.get_cell(1, 8).set_pencil_values({6}, rule_solve=True)
        self.ks1.get_cell(2, 6).set_pencil_values({1}, rule_solve=True)

        self.assertTrue(self.ks1.get_cell(1, 8).get_complete())
        self.assertTrue(self.ks1.get_cell(2, 6).get_complete())
        self.assertEqual(self.ks1.get_cell(1, 8).get_solid_value(), 6)
        self.assertEqual(self.ks1.get_cell(2, 6).get_solid_value(), 1)

        self.ks1.elim_house("box", 2)

        self.assertEqual(self.ks1.get_cell(1, 7).get_pencil_values(), {2, 3, 4, 5, 7, 8, 9})

    def test__elim_using_box__pencil_and_solid_values(self):

        self.ks1.get_cell(3, 0).set_pencil_values({4}, rule_solve=True)
        self.ks1.get_cell(4, 1).set_pencil_values({7}, rule_solve=True)
        self.ks1.get_cell(5, 2).set_pencil_values({4, 7, 9}, rule_solve=True)

        self.assertTrue(self.ks1.get_cell(3, 0).get_complete())
        self.assertTrue(self.ks1.get_cell(4, 1).get_complete())
        self.assertEqual(self.ks1.get_cell(3, 0).get_solid_value(), 4)
        self.assertEqual(self.ks1.get_cell(4, 1).get_solid_value(), 7)

        self.ks1.elim_house("box", 3)

        self.assertEqual(self.ks1.get_cell(5, 2).get_pencil_values(), {9})

        self.assertEqual(self.ks1.get_cell(5, 2).get_solid_value(), 9)

    def test__find_single_candidates__row_with_one_single_candidate(self):

        for cell in self.ks1.find_cells_in_house("row", 0):
            cell.set_pencil_values({2, 3, 4, 5, 6, 7, 8, 9})

        self.ks1.get_cell(0, 0).set_pencil_values({1, 2, 3, 4, 5, 6, 7, 8, 9})

        self.ks1.elim_single_candidates("row", 0)

        self.assertEqual(self.ks1.get_cell(0, 0).get_pencil_values(), {1})
        self.assertEqual(self.ks1.get_cell(0, 3).get_pencil_values(), {2, 3, 4, 5, 6, 7, 8, 9})
        self.assertEqual(self.ks1.get_cell(0, 8).get_pencil_values(), {2, 3, 4, 5, 6, 7, 8, 9})

    def test__find_single_candidates__box_with_one_single_candidate(self):

        for cell in self.ks1.find_cells_in_house("box", 4):
            cell.set_pencil_values({1, 2, 3, 4, 5, 6, 7, 9})

        self.ks1.get_cell(3, 4).set_pencil_values({1, 2, 3, 4, 5, 6, 7, 8, 9})

        self.ks1.elim_single_candidates("box", 4)

        self.assertEqual(self.ks1.get_cell(3, 4).get_pencil_values(), {8})
        self.assertEqual(self.ks1.get_cell(5, 5).get_pencil_values(), {1, 2, 3, 4, 5, 6, 7, 9})

    def test__find_single_candidates__row_with_two_single_candidates(self):

        for cell in self.ks1.find_cells_in_house("row", 0):
            cell.set_pencil_values({2, 3, 5, 6, 7, 8, 9})

        self.ks1.get_cell(0, 0).set_pencil_values({1, 2, 3, 5, 6, 7, 8, 9})
        self.ks1.get_cell(0, 7).set_pencil_values({2, 3, 4, 5, 6, 7, 8, 9})

        self.ks1.elim_single_candidates("row", 0)

        self.assertEqual(self.ks1.get_cell(0, 0).get_pencil_values(), {1})
        self.assertEqual(self.ks1.get_cell(0, 7).get_pencil_values(), {4})
        self.assertEqual(self.ks1.get_cell(0, 3).get_pencil_values(), {2, 3, 5, 6, 7, 8, 9})
        self.assertEqual(self.ks1.get_cell(0, 8).get_pencil_values(), {2, 3, 5, 6, 7, 8, 9})

    def test__find_single_candidates__two_cols_with_different_single_candidates(self):

        for cell in self.ks1.find_cells_in_house("col", 0):
            cell.set_pencil_values({2, 3, 4, 5, 6, 7, 8, 9})

        for cell in self.ks1.find_cells_in_house("col", 1):
            cell.set_pencil_values({1, 2, 3, 5, 6, 7, 8, 9})

        self.ks1.get_cell(2, 0).set_pencil_values({1, 2, 3, 4, 5, 6, 7, 8, 9})
        self.ks1.get_cell(3, 1).set_pencil_values({1, 2, 3, 4, 5, 6, 7, 8, 9})

        self.ks1.elim_single_candidates("col", 0)
        self.ks1.elim_single_candidates("col", 1)

        self.assertEqual(self.ks1.get_cell(2, 0).get_pencil_values(), {1})
        self.assertEqual(self.ks1.get_cell(3, 1).get_pencil_values(), {4})
        self.assertEqual(self.ks1.get_cell(6, 0).get_pencil_values(), {2, 3, 4, 5, 6, 7, 8, 9})
        self.assertEqual(self.ks1.get_cell(8, 1).get_pencil_values(), {1, 2, 3, 5, 6, 7, 8, 9})
        self.assertEqual(self.ks1.get_cell(7, 3).get_pencil_values(), {1, 2, 3, 4, 5, 6, 7, 8, 9})

    def test__eliminate_using_naked_pairs__one_naked_pair_in_row(self):

        self.ks1.get_cell(0, 4).set_pencil_values({1, 2})
        self.ks1.get_cell(0, 6).set_pencil_values({1, 2})
        self.ks1.get_cell(0, 8).set_pencil_values({2, 3, 4, 5, 6, 7, 8, 9})

        self.ks1.elim_naked_pairs("row", 0)

        self.assertEqual(self.ks1.get_cell(0, 0).get_pencil_values(), {3, 4, 5, 6, 7, 8, 9})
        self.assertEqual(self.ks1.get_cell(1, 0).get_pencil_values(), {1, 2, 3, 4, 5, 6, 7, 8, 9})
        self.assertEqual(self.ks1.get_cell(0, 4).get_pencil_values(), {1, 2})
        self.assertEqual(self.ks1.get_cell(0, 6).get_pencil_values(), {1, 2})
        self.assertEqual(self.ks1.get_cell(0, 8).get_pencil_values(), {3, 4, 5, 6, 7, 8, 9})

    def test__eliminate_using_naked_pairs__no_naked_pairs_in_row(self):

        self.ks1.get_cell(3, 4).set_pencil_values({1, 2})
        self.ks1.get_cell(3, 7).set_pencil_values({1, 2, 3})
        self.ks1.get_cell(3, 8).set_pencil_values({2, 3})

        self.ks1.elim_naked_pairs("row", 3)

        self.assertEqual(self.ks1.get_cell(3, 4).get_pencil_values(), {1, 2})
        self.assertEqual(self.ks1.get_cell(3, 7).get_pencil_values(), {1, 2, 3})
        self.assertEqual(self.ks1.get_cell(3, 8).get_pencil_values(), {2, 3})
        self.assertEqual(self.ks1.get_cell(3, 0).get_pencil_values(), {1, 2, 3, 4, 5, 6, 7, 8, 9})

    def test__eliminate_using_naked_pairs__two_naked_pairs_in_box(self):

        self.ks1.get_cell(1, 6).set_pencil_values({5, 7})
        self.ks1.get_cell(1, 7).set_pencil_values({5, 7})
        self.ks1.get_cell(2, 6).set_pencil_values({1, 3})
        self.ks1.get_cell(2, 7).set_pencil_values({1, 3})
        self.ks1.get_cell(0, 7).set_pencil_values({1, 3, 4, 5, 6, 7})

        self.ks1.elim_naked_pairs("box", 2)

        self.assertEqual(self.ks1.get_cell(1, 6).get_pencil_values(), {5, 7})
        self.assertEqual(self.ks1.get_cell(1, 7).get_pencil_values(), {5, 7})
        self.assertEqual(self.ks1.get_cell(2, 6).get_pencil_values(), {1, 3})
        self.assertEqual(self.ks1.get_cell(2, 7).get_pencil_values(), {1, 3})
        self.assertEqual(self.ks1.get_cell(0, 7).get_pencil_values(), {4, 6})


if __name__ == '__main__':

    unittest.main()
