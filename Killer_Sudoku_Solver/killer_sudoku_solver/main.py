import os
import sys
from copy import deepcopy
from datetime import datetime
import itertools
from killer_sudoku_solver.core.SolvingRun import SolvingRun
import killer_sudoku_solver.dks_tool.dks_tool as dks_tool
from importlib.machinery import SourceFileLoader


def write_headers(f):

    f.write("ks_name, "
            "ks_difficulty, "
            "use_rule_solve, "
            "rule_solve_end_condition, "
            "rule_solve_min_rdv, "
            "rule_solve_timeout, "
            "rule_solve_max_blocks, "
            "rule_solve_max_cells, "
            "next_strategy, "
            "search_timeout, "
            "cell_order_option, "
            "minizinc_gen_pseudos, "
            "minizinc_max_blocks, "
            "minizinc_max_cells, "
            "rule_solve_status, "
            "rule_solve_rdvs, "
            "rule_solve_time, "
            "search_solve_status, "
            "search_solve_time, "
            "minizinc_solve_status, "
            "minizinc_solve_time, "
            "gecode_solve_time, "
            "total_solve_time\n\n")


def write_config_and_results(f, config_and_results):

    for config_and_result in config_and_results:

        config_and_result.pop("rule_solve_schedule")
        config_and_result.pop("ks")
        f.write(config_and_result["ks_name"] + ", ")
        config_and_result.pop("ks_name")
        f.write(str(config_and_result["ks_difficulty"]) + ", ")
        config_and_result.pop("ks_difficulty")
        for k, v in config_and_result.items():
            if v is not None:
                f.write(str(v))
            else:
                f.write("n/a")
            if k != list(config_and_result.keys())[-1]:
                f.write(", ")

        f.write("\n")


def write_results(output, start_time, output_option):

    if output_option == "file_per_puzzle":
        for ks_name, config_and_results in output.items():
            file_name = "../data/results/" + ks_name + "_" + start_time.strftime("%Y%m%d_%H%M") + ".csv"
            with open(file_name, "w") as f:

                write_headers(f)
                write_config_and_results(f, config_and_results)
                print("Results file '" + str(f.name) + "' created")

    else:
        file_name = "../data/results/" + "one_file" + "_" + start_time.strftime("%Y%m%d_%H%M") + ".csv"
        with open(file_name, "w") as f:
            write_headers(f)
            for ks_name, config_and_results in output.items():
                write_config_and_results(f, config_and_results)
            print("Results file '" "one_file" + "_" + start_time.strftime("%Y%m%d_%H%M") + ".csv" + "' created in " +
                  "Killer_Sudoku_Solver/killer_sudoku_solver/data/results")


def show_help_screen():

    print("Killer Sudoku Solver")
    print("\nUsage:")
    print("    python ks_solver.py -s")
    print("    python ks_solver.py -m <location of settings file>")
    print("    python ks_solver.py -d")
    print("    python ks_solver.py -l")
    print("    python ks_solver.py --help")
    print("\nOptions:")
    print("    -s       Run solver specifying solving configuration in the console (mode 1)")
    print("    -m       Run solver specifying solving configurations in the settings file (mode 2)")
    print("    -d       Run tool to download puzzles from Daily Killer Sudoku")
    print("    -l       List the names of downloaded puzzles")
    print("    --help   Shows this screen")


def main(args):

    if len(args) == 1:
        print("Error: Argument must be supplied. Showing help screen:")
        show_help_screen()
        sys.exit(1)
    elif args[1] not in ["-s", "-m", "-d", "-l"]:
        if args[1] not in ["--help", "-h"]:
            print("Error: Unknown argument. Showing help screen:")
        show_help_screen()
        sys.exit(1)

    if args[1] == "-s":

        config = dict()

        ks_name = input("Enter the name of the Killer Sudoku to be solved: ")
        try:
            ks = SolvingRun.load_ks(ks_name)
        except FileNotFoundError:
            print("Error: Puzzle file for puzzle '" + ks_name + "' not found. View available puzzles by running solver "
                  "with option -l. Download puzzles from Daily Killer Sudoku by running the solver with option -d.")
            sys.exit(1)

        config["ks"] = ks
        config["use_rule_solve"] = True if input("Execute rule solver? (yes/no): ") == "yes" else False

        if config["use_rule_solve"]:
            config["rule_solve_end_condition"] = \
                input("End condition for rule solve timeout, minimum remaining domain values or none? (t/rdv/none): ")
            if config["rule_solve_end_condition"] == "rdv":
                config["rule_solve_min_rdv"] = int(input("Enter the minimum remaining domain values: "))
                config["rule_solve_timeout"] = None
            elif config["rule_solve_end_condition"] == "t":
                config["rule_solve_timeout"] = int(input("Enter timeout in ms for rule solver: "))
                config["rule_solve_min_rdv"] = None
            else:
                config["rule_solve_timeout"] = None
                config["rule_solve_min_rdv"] = None
            config["rule_solve_schedule"] = "fifo"
            config["rule_solve_max_blocks"] = int(input("Enter maximum blocks to use when finding pseudo cages: "))
            config["rule_solve_max_cells"] = int(input("Enter maximum cells of pseudo cages and cages processed by "
                                                       "cage elimination: "))
        else:
            config["rule_solve_timeout"] = None
            config["rule_solve_min_rdv"] = None
            config["rule_solve_max_blocks"] = None
            config["rule_solve_max_cells"] = None

        config["next_strategy"] = input("Next strategy (backtrack/backjump/fc/minizinc): ")

        if config["next_strategy"] in ["backtrack", "backjump", "fc"]:
            config["search_timeout"] = int(input("Enter timeout for search solve in ms or 0 for no timeout: "))
            if config["next_strategy"] == "fc":
                config["cell_order_option"] = input("Order cells by default or fail-first? (d,f): ")
            else:
                config["cell_order_option"] = None
        elif config["next_strategy"] == "minizinc":
            config["search_timeout"] = None
            config["cell_order_option"] = None
            if not config["use_rule_solve"]:
                config["minizinc_gen_pseudos"] = True if input("Generate pseudo cages before running MiniZinc? "
                                                               "(yes/no): ") == "yes" else False
            else:
                config["minizinc_gen_pseudos"] = None
            if config["minizinc_gen_pseudos"]:
                config["minizinc_max_blocks"] = int(input("Enter maximum blocks to use when finding pseudo cages "
                                                          "before MiniZinc solve is executed: "))
                config["minizinc_max_cells"] = int(input("Enter maximum cells in pseudo cages when finding "
                                                         "pseudo cage before MiniZinc solve is executed: "))
            else:
                config["minizinc_gen_pseudos"] = None
                config["minizinc_max_blocks"] = None
                config["minizinc_max_cells"] = None
        else:
            config["next_strategy"] = None
            config["search_timeout"] = None
            config["cell_order_option"] = None

        solver = SolvingRun(config)
        solver.solve(show_progress=True)

    elif args[1] == "-m":

        if len(args) != 3:
            print("Settings file must be supplied as an argument.")
            print("Usage:")
            print("    python ks_solver.py -m <location of settings file>")
            sys.exit(1)

        start_time = datetime.now()

        settings = SourceFileLoader("settings", sys.argv[2]).load_module()

        multi_config = settings.multi_config
        output_option = multi_config["output_option"]
        multi_config.pop("output_option")

        config_keys = ("ks_name", "use_rule_solve", "rule_solve_end_condition", "rule_solve_min_rdv",
                       "rule_solve_timeout", "rule_solve_max_blocks", "rule_solve_max_cells",
                       "next_strategy", "search_timeout", "cell_order_option", "minizinc_gen_pseudos",
                       "minizinc_max_blocks", "minizinc_max_cells")
        combinations = itertools.product(*(multi_config[x] for x in multi_config))
        configs = []
        for combination in combinations:
            configs.append(dict(zip(config_keys, combination)))

        for config in configs:
            if not config["use_rule_solve"]:
                config["rule_solve_end_condition"] = None
                config["rule_solve_min_rdv"] = None
                config["rule_solve_timeout"] = None
                config["rule_solve_schedule"] = None
                config["rule_solve_max_blocks"] = None
                config["rule_solve_max_cells"] = None
            else:
                config["rule_solve_schedule"] = "fifo"
                if config["rule_solve_end_condition"] == "rdv":
                    config["rule_solve_timeout"] = None
                elif config["rule_solve_end_condition"] == "t":
                    config["rule_solve_min_rdv"] = None
                else:
                    config["rule_solve_end_condition"] = None
                    config["rule_solve_timeout"] = None
                    config["rule_solve_min_rdv"] = None
                config["minizinc_gen_pseudos"] = None
                config["minizinc_max_blocks"] = None
                config["minizinc_max_cells"] = None
            if not config["next_strategy"] in ["backtrack", "backjump", "fc"]:
                config["search_timeout"] = None
                config["cell_order_option"] = None
            if not config["next_strategy"] == "fc":
                config["cell_order_option"] = None
            if config["next_strategy"] == "minizinc":
                if not config["minizinc_gen_pseudos"]:
                    config["minizinc_max_blocks"] = None
                    config["minizinc_max_cells"] = None
            else:
                config["minizinc_max_blocks"] = None
                config["minizinc_max_cells"] = None

        new_configs = []
        for config in configs:
            if config not in new_configs:
                new_configs.append(config)
        configs = new_configs

        print(str(len(configs)) + " solving configurations queued.")

        loaded_kss = dict()
        for config in configs:
            if config["ks_name"] not in loaded_kss.keys():
                loaded_kss[config["ks_name"]] = SolvingRun.load_ks(config["ks_name"])
            config["ks"] = deepcopy(loaded_kss[config["ks_name"]])
            config["ks_difficulty"] = config["ks"].difficulty

        output = dict()
        for config in configs:

            print("Running configuration " + str(configs.index(config) + 1) + "/" + str(len(configs)) + "...")

            solver = SolvingRun(config)
            result = solver.solve(show_progress=False)

            config_and_result = config
            config_and_result.update(result)

            if config_and_result["ks_name"] not in output:
                output[config_and_result["ks_name"]] = [config_and_result]
            else:
                output[config_and_result["ks_name"]].append(config_and_result)

        write_results(output, start_time, output_option)

    elif args[1] == "-d":

        print("Running Daily Killer Sudoku puzzle downloader tool. ")
        puzzle_ids_string = input("Enter the puzzle id/ids separated by a comma you wish to download from Daily Killer "
                                  "Sudoku: ")
        puzzle_ids_string.replace(' ', '')
        puzzle_ids = puzzle_ids_string.split(',')

        dks_tool.main(puzzle_ids)

    elif args[1] == "-l":

        print("Listing all downloaded puzzles: ")

        abs_path = os.path.abspath(__file__)
        dir_name = os.path.dirname(abs_path)
        os.chdir(dir_name)

        l = os.listdir("data/ks_puzzles/")
        l = [x.replace('.csv', '') for x in l if "dks" in x]
        for puzzle_id in l:
            print(puzzle_id)