import datetime
import time
import minizinc
import sys
import os
import csv
import numpy as np
from killer_sudoku_solver.core.classes import KillerSudoku
from killer_sudoku_solver.core.searchers import backtrack, backjump, forward_checking
from killer_sudoku_solver.core.EventManager import EventManager


class SolvingRun:

    def __init__(self, config):

        self.config = config
        self.result = {
            "rule_solve_status": None,
            "rule_solver_rdv": None,
            "rule_solve_time": None,
            "search_solve_status": None,
            "search_solve_time": None,
            "minizinc_solve_status": None,
            "minizinc_solve_time": None,
            "gecode_solve_time": None,
            "total_solve_time": None
        }

    @staticmethod
    def load_ks(ks_name):
        """Creates a Killer Sudoku object by loading the specified puzzle file"""

        abs_path = os.path.abspath(__file__)
        dir_name = os.path.dirname(abs_path)
        os.chdir(dir_name)

        cages_list = []
        with open("../data/ks_puzzles/" + ks_name + ".csv") as f:
            lines = csv.reader(f)
            difficulty = int(next(lines)[0])
            for line in lines:
                cage = []
                cage_total = int(line[1])
                cage_cells = []
                cage_cells_data = line[0]
                for cell_data in cage_cells_data.split(","):
                    coords = cell_data.split("|")
                    cell_x_coord = int(coords[0])
                    cell_y_coord = int(coords[1])
                    cage_cells.append((cell_x_coord, cell_y_coord))
                cage.append(cage_cells)
                cage.append(cage_total)
                cages_list.append(cage)

        cages_np = np.array(cages_list)
        ks = KillerSudoku.KillerSudoku(cages_np, difficulty)

        return ks

    def solve(self, show_progress):
        """Executes a solving run with the configuration specified self.config. Compiles results of the solve in
        self.result."""

        if not show_progress:
            sys.stdout = open(os.devnull, "w")

        if self.config["use_rule_solve"]:
            print("\nPerforming rule solve...")
            self.rule_solve()

            if self.result["rule_solve_status"] == "timed_out":
                print("Rule solver timed out, empty cells remaining: \n")
            elif self.result["rule_solve_status"] == "min_rdv_reached":
                print("Maximum remaining domain values reached. Current state of puzzle: \n")
            else:
                self.result["rule_solve_status"] = self.config["ks"].solved()
                if self.result["rule_solve_status"] is True:
                    print("Rule solve completed puzzle: \n")

                    # Rule solve has solved the instance, so no need to state next strategy configuration options in
                    # output
                    self.config["next_strategy"] = None
                    self.config["search_timeout"] = None
                    self.config["cell_order_option"] = None
                else:
                    print("Rule solver failed to complete puzzle, empty cells remaining: \n")

            self.config["ks"].print_solid_values()

        if self.result["rule_solve_status"] is not True and self.config["next_strategy"] is not None:

            if self.config["next_strategy"] in ["backtrack", "backjump", "fc"]:
                print("\nPerforming " + self.config["next_strategy"] + " search...")
                self.search_solve()
                if self.result["search_solve_status"] == "timed_out":
                    print("Search solve timed out \n")
                else:
                    self.result["search_solve_status"] = self.config["ks"].solved()
                    if self.result["search_solve_status"] is True:
                        print("Search solve found a solution: \n")
                    else:
                        print("Search solve failed: \n")

            elif self.config["next_strategy"] == "minizinc":
                print("\nSolving with minizinc... ")
                self.minizinc_solve()
                self.result["minizinc_solve_status"] = self.config["ks"].solved()
                if self.result["minizinc_solve_status"]:
                    print("Minizinc solve found a solution: \n")
                else:
                    print("Minizinc solve failed: \n")

            self.config["ks"].print_solid_values()

        for key in ["rule_solve_time", "search_solve_time", "minizinc_solve_time"]:
            if self.result[key] is not None:
                if self.result["total_solve_time"] is None:
                    self.result["total_solve_time"] = 0
                self.result["total_solve_time"] += self.result[key]

        if self.result["rule_solve_time"] or self.result["search_solve_time"] or self.result["minizinc_solve_time"]:
            print("\nTimings: \n")
            if self.config["use_rule_solve"]:
                print("Rule solve time: " + str(self.result["rule_solve_time"]) + "ms")
            if self.config["next_strategy"] in ["backtrack", "backjump", "fc"]:
                if self.result["search_solve_time"]:
                    print("Search time: " + str(self.result["search_solve_time"]) + "ms")
            if self.config["next_strategy"] == "minizinc":
                print("Minizinc time: " + str(self.result["minizinc_solve_time"]) + "ms")

            print("Total solve time: " + str(self.result["total_solve_time"]) + "ms")

        sys.stdout = sys.__stdout__

        return self.result

    def rule_solve(self):
        """Executes rule solve with the configuration specified in self.config. Compiles results of the solve in
        self.result."""

        rule_solve_start_time = time.time()
        if self.config["rule_solve_timeout"]:
            max_time = rule_solve_start_time + (self.config["rule_solve_timeout"] / 1000)
        ks = self.config["ks"]

        em = EventManager(self.config)
        eliminations_made = True

        while eliminations_made:

            total_pencil_values = 0
            for cell in ks.cells:
                if not cell.complete:
                    total_pencil_values += len(cell.pencil_values)

            if self.config["rule_solve_min_rdv"]:
                if total_pencil_values < self.config["rule_solve_min_rdv"]:
                    self.result["rule_solve_status"] = "min_rdv_reached"
                    break

            if self.config["rule_solve_timeout"]:
                if time.time() > max_time:
                    self.result["rule_solve_status"] = "timed_out"
                    break

            eliminations_made = em.execute()

        self.result["rule_solver_rdv"] = total_pencil_values
        self.result["rule_solve_time"] = round((time.time() - rule_solve_start_time) * 1000)

    def search_solve(self):
        """Executes search solve with the configuration specified in self.config. Compiles results of the solve in
        self.result."""

        search_solve_start_time = time.time()

        ks = self.config["ks"]
        search_status = False

        # Set timeout variables if a timeout is set
        start_time = None
        timeout = None
        if self.config["search_timeout"]:
            start_time = datetime.datetime.now()
            timeout = datetime.timedelta(milliseconds=self.config["search_timeout"])

        if self.config["cell_order_option"] == "f":
            ks.sort_cells(scheme="fail_first")

        if self.config["next_strategy"] == "backtrack":

            search_status = backtrack.backtrack_solve(ks=ks, start_time=start_time, timeout=timeout)

        elif self.config["next_strategy"] == "backjump":

            # All solid cells must be made into variables with one element in their domain for backjump to work
            for cell in ks.get_cells():
                if cell.get_complete():
                    cell.set_pencil_values({cell.get_solid_value()})
                    cell.set_solid_value(0)

            search_status = backjump.backjump_solve(ks=ks, start_time=start_time, timeout=timeout, i=0)

        elif self.config["next_strategy"] == "fc":

            # Before running forward checking, make all cell's arc consistent

            for i in range(0, 9):
                ks.elim_house("row", i, rule_solve=False)
            for i in range(0, 9):
                ks.elim_house("col", i, rule_solve=False)
            for i in range(0, 9):
                ks.elim_house("box", i, rule_solve=False)

            for cage in ks.get_current_cages():

                combinations = ks.cage_combinations_loader.get_unique_combinations(cage.get_clue(), len(cage.get_cells()))
                allowed_values = set()
                for combination in combinations:
                    allowed_values = allowed_values.union(set(combination))

                for cell in cage.get_cells():
                    new_pencil_values = set(cell.get_pencil_values()).intersection(allowed_values)
                    cell.set_pencil_values(new_pencil_values)

            search_status, ks = forward_checking.fc_solve(ks=ks, start_time=start_time, timeout=timeout)

            if self.config["cell_order_option"] == "f" or self.config["cell_order_option"] == "l":
                ks.sort_cells(scheme="default")

        else:

            print("Error: Search technique not found.")

        if search_status == "solved":
            search_status = True

        self.result["search_solve_status"] = search_status
        self.result["search_solve_time"] = round((time.time() - search_solve_start_time) * 1000)

    def minizinc_solve(self):
        """Executes minizinc solve with the configuration specified in self.config. Compiles results of the solve in
        self.result.

        THIS METHOD CONTAINS CODE TAKEN FROM THE DOCUMENT 'A MiniZinc Tutorial', PAGE 30. THE DOCUMENT IS FOUND HERE:
        https://www.minizinc.org/tutorial/minizinc-tute.pdf"""

        minizinc_solve_start_time = time.time()

        ks = self.config["ks"]

        model = minizinc.Model()

        # CODE FROM 'A MiniZinc Tutorial'
        model.add_string("""
            % sudoku solver
            include "alldifferent.mzn";

            int: S = 3;
            int: N = S * S;
            int: digs = ceil(log(10.0, int2float(N))); % digits for output

            set of int: PuzzleRange = 1..N;
            set of int: SubSquareRange = 1..S;
        """)

        # CODE FROM 'A MiniZinc Tutorial'
        model.add_string("array[1..N, 1..N] of 0..N: start = [|")

        solid_string = ""
        for cell in ks.get_cells():
            solid_string += str(cell.get_solid_value())
            if cell.get_col() != 8:
                solid_string += ", "
            else:
                solid_string += "|"
                if cell.get_row() != 8:
                    solid_string += "\n"
                else:
                    solid_string += "];\n\n"

        model.add_string(solid_string)

        # CODE FROM 'A MiniZinc Tutorial'
        model.add_string("""
            array[1..N, 1..N] of var PuzzleRange: puzzle;

            % fill initial grid
            constraint forall(i,j in PuzzleRange)(
              if start[i,j] > 0 then puzzle[i,j] = start[i,j] else true endif );

            % all different in rows
            constraint forall(i,j in PuzzleRange)(
              alldifferent([puzzle[i,j] | j in PuzzleRange]));

            % all different in columns
            constraint forall(i,j in PuzzleRange)(
              alldifferent([puzzle[i,j] | i in PuzzleRange]));

            % all different in sub-squares
            constraint forall(a, o in SubSquareRange)(
              alldifferent([puzzle[(a-1) *S + a1, (o-1)*S + o1] | a1, o1 in SubSquareRange]));
        """)

        if self.config["minizinc_gen_pseudos"]:
            for i in range(1, (self.config["minizinc_max_blocks"] + 1)):
                ks.find_pseudo_cages(houses_block_count=i,
                                     max_cells_in_cage=self.config["minizinc_max_cells"])
            ks.assign_combined_cages()

        ks.assign_combined_cages()
        model.add_string(ks.generate_minizinc_constraints(include_pseudo_cages=True,
                                                          print_constraints=False))

        solver = minizinc.Solver.lookup("gecode")
        instance = minizinc.Instance(solver, model)

        gecode_solve_start_time = time.time()

        mz_result = instance.solve()

        self.result["gecode_solve_time"] = round((time.time() - gecode_solve_start_time) * 1000)
        self.result["minizinc_solve_time"] = round((time.time() - minizinc_solve_start_time) * 1000)

        if mz_result.status.name == "SATISFIED":
            solution_grid = mz_result.solution.puzzle
            for i in range(0, 9):
                for j in range(0, 9):
                    ks.get_cell(i, j).set_complete(True)
                    ks.get_cell(i, j).set_solid_value(solution_grid[i][j])
