from killer_sudoku_solver.core.classes.Cell import Cell


class Sudoku:

    def __init__(self):

        self.cells = []
        for i in range(0, 9):
            for j in range(0, 9):
                self.cells.append(Cell(i, j))

    def get_cell(self, row, col):
        """Returns the cell in the row and col specified"""

        index = row * 9 + col
        return self.cells[index]

    def get_cells(self):

        return self.cells

    def find_cells_in_house(self, house_type, house_id):
        """Returns all the cells in the specified house"""

        cells_in_house = []

        for cell in self.cells:

            if house_type == "row":
                if cell.get_row() == house_id:
                    cells_in_house.append(cell)

            if house_type == "col":
                if cell.get_col() == house_id:
                    cells_in_house.append(cell)

            if house_type == "box":
                if cell.get_box() == house_id:
                    cells_in_house.append(cell)

        return cells_in_house

    @staticmethod
    def cell_in_houses(cell, house_type, house_ids):
        """If the input cell is any of the input houses returns True. Else returns False."""

        if house_type == "row":
            if cell.get_row() in house_ids:
                return True
        if house_type == "col":
            if cell.get_col() in house_ids:
                return True
        if house_type == "box":
            if cell.get_box() in house_ids:
                return True

        return False

    @staticmethod
    def all_cells_share_house(cells):
        """If any of the input cells share a house, returns True. Else returns False."""

        rows = []
        for cell in cells:
            rows.append(cell.get_row())

        if len(set(rows)) == 1:
            return True

        cols = []
        for cell in cells:
            cols.append(cell.get_col())

        if len(set(cols)) == 1:
            return True

        boxes = []
        for cell in cells:
            boxes.append(cell.get_box())

        if len(set(boxes)) == 1:
            return True

        return False

    def print_solid_values(self):
        """Prints the solid values of the Sudoku. Boxes are separated from each other with spaces and new lines."""

        for i in range(0, 9):
            row_string = ""
            if i % 3 == 0 and i != 0:
                row_string += "\n"
            for j in range(0, 9):
                if j % 3 == 0 and j != 0:
                    row_string += "  "
                row_string += str(self.get_cell(i, j).get_solid_value()) + " "
            print(row_string)

    def print_pencil_values(self):
        """Prints the pencil values of all the Sudoku's cells"""

        for i in range(0, 9):
            row_string = ""
            for j in range(0, 9):
                row_string += str(self.get_cell(i, j).get_pencil_values()) + ",  "
            print(row_string)

    def sort_cells(self, scheme):
        """Orders the cells according to the input scheme. If scheme == "default", cells will be ordered according to
        their index in ascending order (this is the default order). If scheme == "fail-first", cells will be ordered
        according to how many pencil values a cell has, in ascending order."""

        if scheme == "default":

            self.cells.sort(key=lambda cell: cell.find_index())

        if scheme == "fail_first":

            self.cells.sort(key=lambda cell: len(cell.pencil_values))

    # Eliminate candidates by using the solid values of other cells in the same house (row/col/box)
    def elim_house(self, house_type, house_id, rule_solve=True):
        """Implements the human technique 'scanning', described here:
        https://www.conceptispuzzles.com/index.aspx?uri=puzzle/sudoku/techniques"""

        cells_in_house = self.find_cells_in_house(house_type, house_id)

        house_values = []
        for cell in cells_in_house:
            if cell.get_complete():
                house_values.append(cell.get_solid_value())

        if len(house_values) != 0:
            for cell in cells_in_house:
                if not cell.get_complete():
                    cell.remove_pencil_values(house_values, rule_solve=rule_solve)

    def elim_single_candidates(self, house_type, house_id):
        """Implements the human technique 'single candidates', described here:
        https://www.conceptispuzzles.com/index.aspx?uri=puzzle/sudoku/techniques"""

        cells_in_house = self.find_cells_in_house(house_type, house_id)
        value_count = dict()

        for cell in cells_in_house:
            for value in cell.get_pencil_values():
                value_count[value] = value_count.get(value, 0) + 1

        single_candidate_values = []
        for value, count in value_count.items():
            if count == 1:
                single_candidate_values.append(value)

        for value in single_candidate_values:
            for cell in cells_in_house:
                if value in cell.get_pencil_values():
                    cell.set_pencil_values({value}, rule_solve=True)

    def elim_naked_pairs(self, house_type, house_id):
        """Implements the human technique 'naked pairs', described here:
        https://www.conceptispuzzles.com/index.aspx?uri=puzzle/sudoku/techniques"""

        cells_in_house = self.find_cells_in_house(house_type, house_id)
        pairs_count = dict()

        for cell in cells_in_house:
            cell_pencil_values = cell.get_pencil_values()
            if len(cell_pencil_values) == 2:
                pairs_count[frozenset(cell_pencil_values)] = pairs_count.get(frozenset(cell_pencil_values), 0) + 1

        for pair_of_values, count in pairs_count.items():
            if count == 2:
                for cell in cells_in_house:
                    if cell.get_pencil_values() != pair_of_values:
                        cell.remove_pencil_values(pair_of_values, rule_solve=True)
