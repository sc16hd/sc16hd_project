import itertools
from copy import copy, deepcopy

from killer_sudoku_solver.core.classes.Cage import Cage
from killer_sudoku_solver.core.classes.loaders import BoxGroupLoader, CombinationsLoader
from killer_sudoku_solver.core.classes.Sudoku import Sudoku


class KillerSudoku(Sudoku):

    def __init__(self, input_cages, difficulty=None):

        super().__init__()

        self.original_cages = []
        self.current_cages = []
        for cage in input_cages:
            self.original_cages.append(Cage(self, cage[0], cage[1], "original"))
            self.current_cages.append(Cage(self, cage[0], cage[1], "current"))

        self.pseudo_cages = []

        # Combined cages will be used for the union of current cages and pseudo cages, initialise it to an empty list
        self.combined_cages = []

        self.difficulty = difficulty

        self.box_group_loader = BoxGroupLoader.BoxGroupLoader()
        self.cage_combinations_loader = CombinationsLoader.CombinationsLoader()

    def get_original_cages(self):

        return self.original_cages

    def get_current_cages(self):

        for cage in self.current_cages:
            if len(cage.get_cells()) == 0:
                self.current_cages.remove(cage)
        return self.current_cages

    def get_pseudo_cages(self):

        return self.pseudo_cages

    def get_combined_cages(self):

        return self.combined_cages

    def set_current_cages(self, current_cages):

        self.current_cages = current_cages

    def set_pseudo_cages(self, pseudo_cages):

        self.pseudo_cages = pseudo_cages

    def set_combined_cages(self, combined_cages):

        self.combined_cages = combined_cages

    def solved(self):
        """Method to test if the Killer Sudoku is solved. Returns True if solved, False if not."""

        complete_set = {1, 2, 3, 4, 5, 6, 7, 8, 9}

        # Check the numbers 1-9 are in all rows, columns and boxes
        for house in ["row", "col", "box"]:
            for i in range(0, 9):
                house_values = set()
                house_cells = self.find_cells_in_house(house, i)
                for cell in house_cells:
                    house_values.add(cell.get_solid_value())
                if house_values != complete_set:
                    return False

        # Check the cages are satisfied
        for cage in self.get_original_cages():
            cage_cells_solid_values = []
            for cell in cage.get_cells():
                cage_cells_solid_values.append(cell.get_solid_value())

            # Check cage total is satisfied
            if sum(cage_cells_solid_values) != cage.get_clue():
                return False

            # Check cage values are all unique
            if len(cage_cells_solid_values) != len(set(cage_cells_solid_values)):
                return False

        return True

    # Gets the current state of the puzzle in terms of minizinc constraints
    def generate_minizinc_constraints(self, include_pseudo_cages=False, print_constraints=False):
        """Creates constraints for the puzzle's cages in the MiniZinc language. If include_pseudo_cages is True,
        constraints will be generated for pseudo cages as well as original cages."""

        if include_pseudo_cages:
            cages = self.get_combined_cages()
        else:
            cages = self.get_current_cages()

        all_constraints_string = ""
        for cage in cages:
            constraint_total_string = "constraint "
            constraint_alldiff_string = "constraint alldifferent(["
            cage_cells = cage.get_cells()
            if len(cage_cells) == 0:
                continue
            for cell in cage_cells:
                puzzle_string = "puzzle[" + str(cell.get_row() + 1) + ", " + str(cell.get_col() + 1) + "]"
                constraint_total_string += puzzle_string
                constraint_alldiff_string += puzzle_string
                if cell != cage_cells[len(cage_cells) - 1]:
                    constraint_total_string += " + "
                    constraint_alldiff_string += ", "
            constraint_total_string += " = " + str(cage.get_clue()) + ";\n"
            constraint_alldiff_string += "]);\n"
            all_constraints_string += constraint_total_string
            if cage.get_unique_combinations():
                all_constraints_string += constraint_alldiff_string
        for cell in self.get_cells():
            cell_pencil_values = cell.get_pencil_values()
            constraint_cell_string = ("constraint puzzle[" + str(cell.get_row() + 1) + ", " + str(cell.get_col()
                                                                                                  + 1) + "]")
            if not cell.get_complete():
                constraint_cell_string += " in {"
                cell_pencil_values_list = list(cell_pencil_values)
                for pencil_value in cell_pencil_values_list:
                    if not pencil_value == cell_pencil_values_list[len(cell_pencil_values_list) - 1]:
                        constraint_cell_string += (str(pencil_value) + ", ")
                    else:
                        constraint_cell_string += (str(pencil_value) + "};\n")
            else:
                constraint_cell_string += (" = " + str(cell.get_solid_value()) + ";\n")
            all_constraints_string += constraint_cell_string

        if print_constraints:
            print(all_constraints_string)

        return all_constraints_string

    def find_current_cages_in_houses(self, house_type, house_ids):
        """For a given house, returns all current cages that have at least one cell in the house."""

        cages_in_houses = []

        for cage in self.get_current_cages():
            for cell in cage.get_cells():
                if house_type == "row":
                    if cell.get_row() in house_ids:
                        cages_in_houses.append(cage)
                        break
                if house_type == "col":
                    if cell.get_col() in house_ids:
                        cages_in_houses.append(cage)
                        break
                if house_type == "box":
                    if cell.get_box() in house_ids:
                        cages_in_houses.append(cage)
                        break

        return cages_in_houses

    # Given a row, col or box along with an id, returns the cages that have a cage in the row, col or box with the id
    # that have a cell that lies outside the row, col or box
    def find_current_cages_with_cell_outside_houses(self, house_type, house_ids):
        """For a given house, returns the current cages that have at least one cell in the house and at least one
        cell outside of the house"""

        cages_in_houses = self.find_current_cages_in_houses(house_type, house_ids)
        cages_with_cell_outside_houses = []

        for cage in cages_in_houses:
            for cell in cage.get_cells():
                if house_type == "row":
                    if cell.get_row() not in house_ids:
                        cages_with_cell_outside_houses.append(cage)
                        break
                elif house_type == "col":
                    if cell.get_col() not in house_ids:
                        cages_with_cell_outside_houses.append(cage)
                        break
                elif house_type == "box":
                    if cell.get_box() not in house_ids:
                        cages_with_cell_outside_houses.append(cage)
                        break

        return cages_with_cell_outside_houses

    def find_innies_for_houses(self, house_type, house_ids):
        """Returns the innie cells for a given house"""

        cages_with_outies = self.find_current_cages_with_cell_outside_houses(house_type, house_ids)
        innies = []

        for cage in cages_with_outies:
            for cell in cage.get_cells():
                if self.cell_in_houses(cell, house_type, house_ids):
                    innies.append(cell)

        return innies

    def find_outies_for_houses(self, house_type, house_ids):
        """Returns the outie cells for a given house"""

        cages_with_outies = self.find_current_cages_with_cell_outside_houses(house_type, house_ids)
        outies = []

        for cage in cages_with_outies:
            for cell in cage.get_cells():
                if not self.cell_in_houses(cell, house_type, house_ids):
                    outies.append(cell)

        return outies


    def find_pseudo_cages(self, houses_block_count, max_cells_in_cage):
        """Finds pseudo cages and saves them to self.pseudo_cages. Only pseudo cages with at most max_cells_in_cage
        cells will be saved. houses_block_count determines the number of houses to group together when finding pseudo
        cages."""

        pseudo_cages = []

        id_range = 10 - houses_block_count
        houses_sum = 45 * houses_block_count

        for house_type in ["row", "col", "box"]:
            if house_type != "box":
                for id in range(0, id_range):
                    ids = []
                    for i in range(0, houses_block_count):
                        ids.append(id + i)
                    self.find_innie_and_outie_cage(house_type, ids, houses_sum, pseudo_cages, max_cells_in_cage)
            if house_type == "box":
                done_groups = []
                for id in range(0, 9):
                    box_groups = self.box_group_loader.get_box_groups(id, houses_block_count)
                    for ids in box_groups:
                        if set(ids) not in done_groups:
                            done_groups.append(set(ids))
                            self.find_innie_and_outie_cage(house_type, ids, houses_sum, pseudo_cages, max_cells_in_cage)

        self.pseudo_cages.extend(pseudo_cages)

    def find_innie_and_outie_cage(self, house_type, house_ids, houses_sum, pseudo_cages, max_cells_in_cage):
        """For a given house group, returns the innie and outie cage. The house group is determined by the house_type
        and house_ids. Only pseudo cages with at most max_cells_in_cage cells will be returned."""

        outies_for_houses = self.find_outies_for_houses(house_type, house_ids)
        innies_for_houses = self.find_innies_for_houses(house_type, house_ids)

        if len(outies_for_houses) == 0:
            return

        if len(outies_for_houses) > max_cells_in_cage and len(innies_for_houses) > max_cells_in_cage:
            return

        cages_with_outies = self.find_current_cages_with_cell_outside_houses(house_type, house_ids)
        cages_with_outies_total = 0
        for cage in cages_with_outies:
            cages_with_outies_total += cage.get_clue()

        cages_with_no_outies = [cage for cage in self.find_current_cages_in_houses(house_type, house_ids) if
                                cage not in cages_with_outies]
        cages_with_no_outies_total = 0
        for cage in cages_with_no_outies:
            cages_with_no_outies_total += cage.get_clue()

        cells_in_houses = []
        for id in house_ids:
            cells_in_houses.extend(self.find_cells_in_house(house_type, id))

        houses_total = 0
        for cell in cells_in_houses:
            houses_total += cell.get_solid_value()

        new_innie_cage_total = (houses_sum - houses_total) - cages_with_no_outies_total
        innies_share_house = KillerSudoku.all_cells_share_house(innies_for_houses)
        new_innie_cage = Cage(self, innies_for_houses, new_innie_cage_total, "pseudo", innies_share_house)

        new_outie_cage_total = cages_with_outies_total - new_innie_cage_total
        outies_share_house = KillerSudoku.all_cells_share_house(outies_for_houses)
        new_outie_cage = Cage(self, outies_for_houses, new_outie_cage_total, "pseudo", outies_share_house)

        if len(new_innie_cage.get_cells()) <= max_cells_in_cage:
            pseudo_cages.append(new_innie_cage)

        if len(new_outie_cage.get_cells()) <= max_cells_in_cage:
            pseudo_cages.append(new_outie_cage)

        return pseudo_cages

    def assign_combined_cages(self):
        """Compiles the current and pseudo cages into one list and saves it to self.combined cages."""

        combined_cages = self.get_current_cages().copy()
        combined_cages.extend(self.get_pseudo_cages())
        combined_cages = list(dict.fromkeys(combined_cages))
        self.set_combined_cages(combined_cages)

    def elim_combined_cages(self):
        """Propagator for cage elimination."""

        for cage in self.get_combined_cages():

            # Check if cage is empty
            cell_in_cage_modified = False
            for cell in cage.get_cells():
                if len(cell.get_pencil_values()) == 1:
                    cell_in_cage_modified = True
                    break

            # Only eliminate using empty cages
            if not cell_in_cage_modified:
                pencil_values_in_all_cage_cells = []
                for cell in cage.get_cells():
                    pencil_values_in_all_cage_cells.extend(list(cell.get_pencil_values()))
                pencil_values_in_all_cage_cells_set = set(pencil_values_in_all_cage_cells)

                # Find the sum factors for the cage that are possible given the combined pencil values
                allowed_combinations = []

                if cage.get_unique_combinations():
                    for combination in self.cage_combinations_loader.get_unique_combinations(cage.get_clue(),
                                                                                             len(cage.get_cells())):
                        if len(set(combination).union(pencil_values_in_all_cage_cells_set)) == len(
                                pencil_values_in_all_cage_cells_set):
                            allowed_combinations.append(set(combination))
                else:
                    for combination in self.cage_combinations_loader.get_non_unique_combinations(cage.get_clue(),
                                                                                                 len(cage.get_cells())):
                        if len(set(combination).union(pencil_values_in_all_cage_cells_set)) == \
                                len(pencil_values_in_all_cage_cells_set):
                            allowed_combinations.append(combination)

                # The only candidates for the cells in the cage are the values in the possible combinations
                all_values_in_combinations = []
                for combination in allowed_combinations:
                    all_values_in_combinations.extend(list(combination))

                if len(all_values_in_combinations) == 0:
                    continue

                # Find the values that will be eliminated from every cell in the cage
                all_values_in_combinations_set = set(all_values_in_combinations)
                numbers_1_to_9_set = {1, 2, 3, 4, 5, 6, 7, 8, 9}
                numbers_to_remove = numbers_1_to_9_set - all_values_in_combinations_set

                # Eliminate these values from the cells
                for cell in cage.get_cells():
                    cell.remove_pencil_values(list(numbers_to_remove), rule_solve=True)

    def elim_combined_cages_improved(self, max_blocks, max_cells_in_cage):
        """Propagator for cage elimination."""

        self.pseudo_cages = []
        for i in range(1, (max_blocks + 1)):
            self.find_pseudo_cages(houses_block_count=i,
                                   max_cells_in_cage=max_cells_in_cage)

        self.assign_combined_cages()

        for cage in self.get_combined_cages():

            if len(cage.get_cells()) <= max_cells_in_cage:

                # Check if cage is empty
                cell_in_cage_modified = False
                for cell in cage.get_cells():
                    if cell.get_complete():
                        cell_in_cage_modified = True
                        break

                # Only eliminate using empty cages
                if not cell_in_cage_modified:
                    pencil_values_in_all_cage_cells = []
                    for cell in cage.get_cells():
                        pencil_values_in_all_cage_cells.extend(list(cell.get_pencil_values()))
                    pencil_values_in_all_cage_cells_set = set(pencil_values_in_all_cage_cells)

                    # Find the sum factors for the cage that are possible given the combined pencil values
                    allowed_combinations = []

                    if cage.get_unique_combinations():
                        for combination in self.cage_combinations_loader.get_unique_combinations(cage.get_clue(),
                                                                                                 len(cage.get_cells())):
                            if len(set(combination).union(pencil_values_in_all_cage_cells_set)) == len(
                                    pencil_values_in_all_cage_cells_set):
                                allowed_combinations.append(set(combination))
                    else:
                        for combination in self.cage_combinations_loader.get_non_unique_combinations(cage.get_clue(),
                                                                                                     len(cage.get_cells())):
                            if len(set(combination).union(pencil_values_in_all_cage_cells_set)) == \
                                    len(pencil_values_in_all_cage_cells_set):
                                allowed_combinations.append(combination)

                    combination_permutations_list = []
                    for combination in allowed_combinations:
                        combination_permutations_list.append(itertools.permutations(combination, len(cage.get_cells())))

                    cage_cells = cage.get_cells()
                    allowed_values = dict()
                    for cell in cage_cells:
                        allowed_values[cell] = set()
                    for permutations in combination_permutations_list:
                        valid_permutations = []
                        for permutation in permutations:
                            permutation_list = list(permutation)
                            permutation_valid = True
                            for i in range(0, len(cage_cells)):
                                if permutation_list[i] not in cage_cells[i].get_pencil_values():
                                    permutation_valid = False
                                    break
                            if permutation_valid:
                                valid_permutations.append(permutation_list)
                        for valid_permutation in valid_permutations:
                            for i in range(0, len(cage_cells)):
                                set_for_cell = allowed_values[cage_cells[i]]
                                set_for_cell.add(valid_permutation[i])

                    for cell, values in allowed_values.items():
                        cell.set_pencil_values(set(values), rule_solve=True)

    def __deepcopy__(self, memo=None):
        """Creates a deepcopy of a Killer Sudoku object. self.box_group_loader and self.cage_combinations_loader is
        shallow copied, as there is no reason for the .csv files to be reloaded."""

        # Create a blank KS
        ks_cls = self.__class__
        copy_ks = ks_cls.__new__(ks_cls)

        # Create deep copies of the cages
        copy_ks.original_cages = []
        copy_ks.current_cages = []
        for cage in self.get_original_cages():
            cage_copy = deepcopy(cage)
            copy_ks.original_cages.append(cage_copy)
            copy_ks.current_cages.append(copy(cage_copy))

        # Find cells by looking at the cells in each cage
        copy_ks.cells = []
        for cage in copy_ks.get_original_cages():
            for cell in cage.get_cells():
                if cell not in copy_ks.cells:
                    copy_ks.cells.append(cell)

        # Replace references of cell's current_cage to the newly created current cage
        for cell in copy_ks.cells:
            for cage in copy_ks.current_cages:
                if cell in cage.cells:
                    cell.current_cage = cage

        # Sort the cells into their original order
        sorted_cells = [None] * 81
        for cell in copy_ks.get_cells():
            sorted_cells[cell.find_index()] = cell
        sorted_cells = [cell for cell in sorted_cells if cell is not None]
        copy_ks.cells = sorted_cells

        # Assign remaining fields
        copy_ks.difficulty = self.difficulty
        copy_ks.box_group_loader = self.box_group_loader
        copy_ks.cage_combinations_loader = self.cage_combinations_loader
        copy_ks.combined_cages = []
        copy_ks.pseudo_cages = []

        return copy_ks
