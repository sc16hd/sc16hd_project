class Cell:

    def __init__(self, input_row, input_col):

        self.row = input_row
        self.col = input_col

        box = None
        if input_row in range(0, 3) and input_col in range(0, 3):
            box = 0
        elif input_row in range(0, 3) and input_col in range(3, 6):
            box = 1
        elif input_row in range(0, 3) and input_col in range(6, 9):
            box = 2
        elif input_row in range(3, 6) and input_col in range(0, 3):
            box = 3
        elif input_row in range(3, 6) and input_col in range(3, 6):
            box = 4
        elif input_row in range(3, 6) and input_col in range(6, 9):
            box = 5
        elif input_row in range(6, 9) and input_col in range(0, 3):
            box = 6
        elif input_row in range(6, 9) and input_col in range(3, 6):
            box = 7
        elif input_row in range(6, 9) and input_col in range(6, 9):
            box = 8

        self.box = box
        self.original_cage = None
        self.current_cage = None
        self.solid_value = 0
        self.pencil_values = {1, 2, 3, 4, 5, 6, 7, 8, 9}
        self.complete = False
        self.pencil_triggered = False
        self.solid_triggered = False

    def get_row(self):

        return self.row

    def get_col(self):

        return self.col

    def get_box(self):

        return self.box

    def get_original_cage(self):

        return self.original_cage

    def get_solid_value(self):

        return self.solid_value

    def get_pencil_values(self):

        return self.pencil_values

    def get_complete(self):

        return self.complete

    def set_solid_value(self, input_solid_value, rule_solve=False):
        """Sets the solid value of the cell. If rule_solve is True, the cell is removed from its current cage. A new
        cage clue is then calculated for the resulting cage, and the solid value of the cell is removed from the
        pencil values of the remaining cells in the cage."""

        self.solid_value = input_solid_value
        if rule_solve:
            self.current_cage.cells.remove(self)
            self.current_cage.clue = self.current_cage.clue - input_solid_value
            for cell in self.current_cage.cells:
                cell.remove_pencil_values([input_solid_value], rule_solve=rule_solve)
            self.current_cage = None

    def set_pencil_values(self, input_pencil_values, rule_solve=False):
        """Sets the pencil values of the cell. If rule_solve is True, self.pencil_triggered is set to True. If the
        cell now has only one pencil value, its solid value can be set to this value, the cell set to complete and
        self.solid_triggered set to True."""

        if not isinstance(input_pencil_values, set):
            raise TypeError("input_pencil_values must be of type 'set'")

        if input_pencil_values != self.pencil_values:
            self.pencil_values = input_pencil_values

            if rule_solve:
                self.pencil_triggered = True
                if not self.get_complete():
                    if len(self.pencil_values) == 1:
                        self.set_solid_value(list(self.pencil_values)[0], rule_solve=rule_solve)
                        self.set_complete(True)
                        self.solid_triggered = True

    def set_complete(self, complete):

        self.complete = complete

    def find_index(self):
        """Finds the index of the cell. Each cell has a unique index 0 - 80."""

        return self.get_row() * 9 + self.get_col()

    def remove_pencil_values(self, values_to_remove, rule_solve=False):
        """Removes the input values_to_remove from the cell's pencil values."""

        pencil_values_list = self.get_pencil_values().copy()
        for value in values_to_remove:
            if value in pencil_values_list:
                pencil_values_list.remove(value)

        self.set_pencil_values(set(pencil_values_list), rule_solve=rule_solve)

    def __eq__(self, obj):
        """Equals method"""

        if not isinstance(obj, Cell):
            return False
        if self.get_row() != obj.get_row():
            return False
        if self.get_col() != obj.get_col():
            return False
        return True

    def __hash__(self):
        """Hash method"""

        return hash((self.get_row(), self.get_col()))

    def __str__(self):
        """String method"""

        return "(" + str(self.get_row()) + ", " + str(self.get_col()) + ")"
