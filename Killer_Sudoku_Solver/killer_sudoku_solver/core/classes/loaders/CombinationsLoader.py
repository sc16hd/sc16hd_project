import os
import killer_sudoku_solver


class CombinationsUnique:
    """Class to represent combinations of unique numbers that sum to a certain value"""

    def __init__(self, number, cage_cells_count, combinations_list):
        self.number = number
        self.cage_cells_count = cage_cells_count
        self.combinations_list = combinations_list


class CombinationsNonUnique:
    """Class to represent combinations of non-unique numbers that sum to a certain value"""

    def __init__(self, number, cage_cells_count, combinations_list):
        self.number = number
        self.cage_cells_count = cage_cells_count
        self.combinations_list = combinations_list


class CombinationsLoader:
    """Class to load the combinations in the files combinations_unique.csv and combinations_non_unique.csv to memory."""

    def __init__(self):

        path = os.path.dirname(killer_sudoku_solver.__file__)

        self.all_unique_combinations = []
        self.all_non_unique_combinations = []

        # Create a combination object for each line read in from both files
        for file_name in ["combinations_unique.csv", "combinations_non_unique.csv"]:
            with open(path + "/core/classes/loaders/" + file_name) as f:
                lines = f.readlines()
                for line in lines:
                    line_list = line.replace("\n", "").split(",")
                    combinations_string_list = line_list[3].split("|")
                    combinations_list = []
                    for combination_string in combinations_string_list:
                        combination = []
                        for value in combination_string.split("-"):
                            combination.append(int(value))
                        combinations_list.append(combination)
                    if len(combinations_list) != 0:
                        if file_name == "combinations_unique.csv":
                            combinations_non_unique_object = CombinationsUnique(int(line_list[1]), int(line_list[2]),
                                                                                combinations_list)
                            self.all_unique_combinations.append(combinations_non_unique_object)
                        if file_name == "combinations_non_unique.csv":
                            combinations_non_unique_object = CombinationsNonUnique(int(line_list[1]), int(line_list[2]),
                                                                                   combinations_list)
                            self.all_non_unique_combinations.append(combinations_non_unique_object)

    def get_unique_combinations(self, number, cage_cells_count):
        """For the given number and cage_cells_coutn, returns all unique combinations of cage_cells_count numbers that
        sum number"""

        for combinations in self.all_unique_combinations:
            if combinations.number == number and combinations.cage_cells_count == cage_cells_count:
                return combinations.combinations_list

        return []

    def get_non_unique_combinations(self, number, cage_cells_count):
        """For the given number and cage_cells_count, returns all combinations, unique and non-unique, of cage_
           cells_count numbers that sum number"""

        for combinations in self.all_non_unique_combinations:
            if combinations.number == number and combinations.cage_cells_count == cage_cells_count:
                return combinations.combinations_list

        return []
