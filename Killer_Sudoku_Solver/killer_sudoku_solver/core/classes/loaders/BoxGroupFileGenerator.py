# File used to generate box combinations file: box_groups.csv

def find_adjacent_boxes(box_id):
    if box_id == 0:
        return [1, 3]
    if box_id == 1:
        return [0, 2, 4]
    if box_id == 2:
        return [1, 5]
    if box_id == 3:
        return [0, 4, 6]
    if box_id == 4:
        return [1, 3, 5, 7]
    if box_id == 5:
        return [2, 4, 8]
    if box_id == 6:
        return [3, 7]
    if box_id == 7:
        return [4, 6, 8]
    if box_id == 8:
        return [5, 7]


def find_box_group(box_id, path_length):

    current_length = 1
    groups = [[box_id]]
    while path_length != current_length:
        new_groups = []
        for path in groups:
            for box in path:
                adjacent_boxes = find_adjacent_boxes(box)
                for box in adjacent_boxes:
                    group_copy = path.copy()
                    group_copy.append(box)
                    if len(group_copy) == len(set(group_copy)):
                        new_groups.append(group_copy)
        groups = new_groups
        current_length += 1

    return groups

if __name__ == "__main__":

        all_box_groups = []

        for i in range(1, 5):
            for j in range(0, 9):
                groups = find_box_group(j, i)
                groups_sets = [frozenset(path) for path in groups]
                groups_sets = list(dict.fromkeys(groups_sets))

                for group in groups_sets:
                    if group not in all_box_groups:
                        all_box_groups.append(group)

        f = open("box_groups.csv", "w")
        for group in all_box_groups:
            write_string = str(set(group)).replace("{", "").replace("}", "").replace(" ", "") + "\n"
            f.write(write_string)
        f.close()