import os
import killer_sudoku_solver


class BoxGroupLoader:
    """Class to load the box groups in the file box_groups.csv to memory."""

    def __init__(self):
        """Load the box groups in box_groups.csv to memory."""

        path = os.path.dirname(killer_sudoku_solver.__file__)
        self.box_groups = []
        with open(path + "/core/classes/loaders/box_groups.csv") as f:
            lines = f.readlines()
            for line in lines:
                box_group_str = line.replace("\n", "").split(",")
                box_group = [int(box) for box in box_group_str]
                self.box_groups.append(box_group)

    def get_box_groups(self, box, length_of_group):
        """Return box groups that contain the given box and are of length length_of_group."""

        box_groups_to_return = []
        for box_group in self.box_groups:
            if len(box_group) == length_of_group and box in box_group:
                box_groups_to_return.append(box_group)

        return box_groups_to_return
