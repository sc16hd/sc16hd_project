# File used to generate combinations files: combinations_unique.csv and combinations_non_unique.csv

def find_cage_combinations_unique_numbers(number, cage_cells_count):

    cage_combinations = []

    if cage_cells_count == 1:
        if 1 <= number <= 9:
            return [{number}]
        else:
            return []

    if cage_cells_count == 2:
        for i in range(1, 10):
            for j in range(1, 10):
                if i + j == number and i != j and {i, j} not in cage_combinations:
                    cage_combinations.append({i, j})

    if cage_cells_count > 2:
        for i in range(1, 10):
            found_combinations = find_cage_combinations_unique_numbers(number - i, cage_cells_count - 1)
            for found_combination in found_combinations:
                if i not in found_combination:
                    found_combination.add(i)
                    if found_combination not in cage_combinations:
                        cage_combinations.append(found_combination)

    return cage_combinations


def find_cage_combinations_non_unique_numbers(number, cage_cells_count):

    cage_combinations = []

    if cage_cells_count == 1:
        if 1 <= number <= 9:
            return [[number]]
        else:
            return []

    if cage_cells_count == 2:
        for i in range(1, 10):
            for j in range(1, 10):
                if i + j == number:
                    cage_combinations.append([i, j])

    if cage_cells_count > 2:
        for i in range(1, 10):
            found_combinations = find_cage_combinations_non_unique_numbers(number - i, cage_cells_count - 1)
            for found_combination in found_combinations:
                if i + sum(found_combination) == number:
                    found_combination.append(i)
                    cage_combinations.append(found_combination)

    for combination in cage_combinations:
        list.sort(combination)

    # Remove duplicates
    cage_combinations = [list(a) for a in set(tuple(combination) for combination in cage_combinations)]

    return cage_combinations


if __name__ == "__main__":

    # for file_name in ["combinations_non_unique_2.csv"]:
    for file_name in ["combinations_unique_2.csv", "combinations_non_unique_2.csv"]:
        combinations_strings = []
        for i in range(1, 46):
            for j in range(1, 9):
                print("i: " + str(i) + ", j: " + str(j))
                if file_name == "combinations_unique_2.csv":
                    combinations = find_cage_combinations_unique_numbers(i, j)
                else:
                    combinations = find_cage_combinations_non_unique_numbers(i, j)
                line_string = "n," + str(i) + "," + str(j) + ","
                if combinations:
                    for combination in combinations:
                        for value in combination:
                            line_string += (str(value) + "-")
                        line_string = line_string[:-1]
                        line_string += "|"
                    line_string = line_string[:-1]
                    line_string += "\n"
                    combinations_strings.append(line_string)

                    with open(file_name, "a") as f:
                        f.write(line_string)
