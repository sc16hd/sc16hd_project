from killer_sudoku_solver.core.classes import Cell


class Cage:

    def __init__(self, ks, cells, clue, cage_type, unique_combinations=True):

        if type(cells[0]) == tuple:
            created_cells = []
            for input_cell in cells:
                created_cell = ks.get_cell(input_cell[0], input_cell[1])
                if cage_type == "original":
                    created_cell.original_cage = self
                if cage_type == "current":
                    created_cell.current_cage = self
                created_cells.append(created_cell)
            self.cells = created_cells
        elif type(cells[0] == Cell):
            self.cells = cells

        self.clue = clue
        self.unique_combinations = unique_combinations

    def get_cells(self):

        return self.cells

    def get_clue(self):

        return self.clue

    def get_unique_combinations(self):

        return self.unique_combinations

    def sum(self):
        """Returns the sum of the cells in the current cage"""

        cage_sum = 0
        for cell in self.get_cells():
            cage_sum += cell.get_solid_value()

        return cage_sum

    def cage_full(self):
        """If all the cage's cells are complete returns True. Else returns False"""

        cage_full = True
        for cell in self.get_cells():
            if not cell.get_complete():
                cage_full = False
                break

        return cage_full

    def __eq__(self, obj):
        """Equals method"""

        if not isinstance(obj, Cage):
            return False
        if self.get_cells() == obj.get_cells() and self.get_clue() == obj.get_clue():
            return True
        return False

    def __hash__(self):
        """Hash method"""

        cells = self.get_cells()
        cell_string = ""
        for cell in cells:
            cell_string += str(cell.get_row())
            cell_string += str(cell.get_col())
        return hash((cell_string, self.get_clue()))

    def __str__(self):
        """String method"""

        cells = self.get_cells()
        cell_string = ""
        for cell in cells:
            cell_string += str(cell)
            if cell != cells[len(cells) - 1]:
                cell_string += ", "
        return "Coords: " + cell_string + ". Total: " + str(self.get_clue())

    def __copy__(self):
        """Shallow copy method"""

        cage_cls = self.__class__
        copy_cage = cage_cls.__new__(cage_cls)

        copy_cage.clue = self.clue
        copy_cage.unique_combinations = self.unique_combinations
        copy_cage.cells = self.cells.copy()

        return copy_cage
