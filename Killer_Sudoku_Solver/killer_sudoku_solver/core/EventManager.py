class EventManager:
    """Class to manage executing and queueing propagators"""

    def __init__(self, config):

        self.schedule = config["rule_solve_schedule"]
        if self.schedule == "fifo" or self.schedule == "lifo":
            self.queue = []
        else:
            self.prio_queue = {
                "elim_house": [],
                "elim_sing_can": [],
                "elim_naked_pairs": [],
                "elim_cage": []
            }
        self.config = config
        self.ks = config["ks"]

        cage_func_call = [self.ks.elim_combined_cages_improved, [2, 2]]

        if self.schedule == "priority":
            self.prio_queue["elim_cage"].append(cage_func_call)
        else:
            self.queue.append(cage_func_call)

    def execute(self):
        """Executes the a propagator from the queue. After the propagator has returned any cell that has been
        triggered is input to the trigger() method."""

        if self.schedule == "priority":
            for queue_key, queue in self.prio_queue.items():
                if len(queue) != 0:
                    func_call = queue.pop(0)
                    break

        else:
            if self.schedule == "fifo":
                func_call = self.queue.pop(0)
            elif self.schedule == "lifo":
                func_call = self.queue.pop()

        func_call[0](*func_call[1])

        for cell in self.ks.cells:
            if cell.pencil_triggered:
                self.trigger(cell)

        if func_call[0] == self.ks.elim_combined_cages_improved:
            cage_func_call = [self.ks.elim_combined_cages_improved, [self.config["rule_solve_max_blocks"],
                                                                     self.config["rule_solve_max_cells"]]]
            if self.schedule == "priority":
                if len(self.prio_queue["elim_house"]) != 0 or len(self.prio_queue["elim_sing_can"]) != 0 or \
                        len(self.prio_queue["elim_naked_pairs"]) != 0:
                    self.prio_queue["elim_cage"].append(cage_func_call)
                    return True
            else:
                if len(self.queue) != 0:
                    if self.schedule == "fifo":
                        self.queue.append(cage_func_call)
                    else:
                        self.queue.insert(0, cage_func_call)
                    return True
        else:
            return True

        return False

    def trigger(self, cell):
        """For the given cell, queues propagators that are 'subscribed' to the cell."""

        if cell.solid_triggered:
            # If a cell has been 'solid triggered', propagators elim_house(), elim_single_candidates() and
            # elim_naked_pairs() should be queued.
            funcs = [self.ks.elim_house, self.ks.elim_single_candidates, self.ks.elim_naked_pairs]
        else:
            # If a cell has been 'pencil triggered' only, only elim_single_candidates() and  elim_naked_pairs() are
            # queued.
            funcs = [self.ks.elim_single_candidates, self.ks.elim_naked_pairs]

        # Create a call to each relevant propagator, and add it to the queue if is not already present in the queue.
        for func in funcs:
            func_call = [func, ["row", cell.get_row()]]
            self.add_to_queue(func_call)

            func_call = [func, ["col", cell.get_col()]]
            self.add_to_queue(func_call)

            func_call = [func, ["box", cell.get_box()]]
            self.add_to_queue(func_call)

        # Reset the triggers
        cell.pencil_triggered = False
        cell.solid_triggered = False

    def add_to_queue(self, func_call):

        if self.schedule == "priority":

            if func_call[0] == self.ks.elim_house:
                key = "elim_house"
            elif func_call[0] == self.ks.elim_single_candidates:
                key = "elim_sing_can"
            else:
                key = "elim_naked_pairs"

            if func_call not in self.prio_queue[key]:
                self.prio_queue[key].append(func_call)

        else:
            if func_call not in self.queue:
                self.queue.append(func_call)
