import os
import datetime
from copy import copy, deepcopy


def clear():
    """Clears the console. Makes the search easier to visualise when running in the console."""

    os.system('clear')


def fc_solve(ks, start_time=None, timeout=None, show_progress=False):
    """Searches the input Killer Sudoku using the forward checking technique."""

    if timeout:
        if (datetime.datetime.now() - start_time).total_seconds() * 1000 > timeout.total_seconds() * 1000:
            return "timed_out", ks

    empty_cell = None
    for cell in ks.get_cells():
        if not cell.get_complete():
            empty_cell = cell
            break

    if empty_cell is None:
        return "solved", ks

    for value in empty_cell.get_pencil_values():

        saved_pencil_values = []
        for cell in ks.get_cells():
            if not cell.get_complete():
                saved_pencil_values.append(deepcopy(cell.get_pencil_values()))
            else:
                saved_pencil_values.append(cell.get_pencil_values())

        if show_progress:
            # input()
            clear()
            print()
            empty_cell.set_solid_value(value)
            ks.print_solid_values()
            empty_cell.set_solid_value(0)
            print()

        if check_value(ks, empty_cell, value):

            empty_cell.set_solid_value(value)
            empty_cell.set_complete(True)

            status, returned_ks = fc_solve(ks, start_time=start_time, timeout=timeout, show_progress=show_progress)
            if status:
                return status, returned_ks

            empty_cell.set_solid_value(0)
            empty_cell.set_complete(False)

        for i in range(0, 81):
            ks.cells[i].pencil_values = saved_pencil_values[i]

    return False, None


def check_value(ks, empty_cell, value):

    row_id = empty_cell.get_row()
    cells_in_row = ks.find_cells_in_house("row", row_id)
    for cell in cells_in_row:
        if cell != empty_cell and not cell.get_complete():
            if value in cell.get_pencil_values():
                cell.remove_pencil_values([value])
                if len(cell.get_pencil_values()) == 0:
                    return False

    col_id = empty_cell.get_col()
    cells_in_col = ks.find_cells_in_house("col", col_id)
    for cell in cells_in_col:
        if cell != empty_cell and not cell.get_complete():
            if value in cell.get_pencil_values():
                cell.remove_pencil_values([value])
                if len(cell.get_pencil_values()) == 0:
                    return False

    box_id = empty_cell.get_box()
    cells_in_box = ks.find_cells_in_house("box", box_id)
    for cell in cells_in_box:
        if cell != empty_cell and not cell.get_complete():
            if value in cell.get_pencil_values():
                cell.remove_pencil_values([value])
                if len(cell.get_pencil_values()) == 0:
                    return False

    empty_cell_cage = None
    for cage in ks.get_current_cages():
        if empty_cell in cage.get_cells():
            empty_cell_cage = cage
            break

    if empty_cell_cage:
        for cell in empty_cell_cage.get_cells():
            if cell != empty_cell and not cell.get_complete():
                if value in cell.get_pencil_values():
                    cell.remove_pencil_values([value])
                    if len(cell.get_pencil_values()) == 0:
                        return False

        new_cage_clue = empty_cell_cage.get_clue() - (empty_cell_cage.sum() + value)

        empty_cells_in_cage = 0
        for cell in empty_cell_cage.get_cells():
            if not cell.get_complete() and cell != empty_cell:
                empty_cells_in_cage += 1

        if empty_cells_in_cage != 0:
            combinations = tuple(ks.cage_combinations_loader.get_unique_combinations(new_cage_clue, empty_cells_in_cage))
            allowed_values = set()
            for combination in combinations:
                allowed_values = allowed_values.union(set(combination))

            for cell in empty_cell_cage.get_cells():
                if not cell.get_complete() and cell != empty_cell:
                    new_pencil_values = set(cell.get_pencil_values()).intersection(allowed_values)
                    if len(new_pencil_values) != 0:
                        cell.set_pencil_values(new_pencil_values)
                    else:
                        return False

    return True


def enter_cell(this_ks, empty_cell, value, allowed_values):

    row_id = empty_cell.get_row()
    cells_in_row = this_ks.find_cells_in_house("row", row_id)
    for cell in cells_in_row:
        if cell != empty_cell and not cell.get_complete():
            cell.remove_pencil_values([value])

    col_id = empty_cell.get_col()
    cells_in_col = this_ks.find_cells_in_house("col", col_id)
    for cell in cells_in_col:
        if cell != empty_cell and not cell.get_complete():
            cell.remove_pencil_values([value])

    box_id = empty_cell.get_box()
    cells_in_box = this_ks.find_cells_in_house("box", box_id)
    for cell in cells_in_box:
        if cell != empty_cell and not cell.get_complete():
            cell.remove_pencil_values([value])

    empty_cell_cage = None
    for cage in this_ks.get_current_cages():
        if empty_cell in cage.get_cells():
            empty_cell_cage = cage
            break

    if allowed_values is not None:
        for cell in empty_cell_cage.get_cells():
            if cell != empty_cell and not cell.get_complete():
                new_pencil_values = set(cell.get_pencil_values()).intersection(allowed_values)
                if value in new_pencil_values:
                    new_pencil_values.remove(value)
                cell.set_pencil_values(new_pencil_values)

    else:
        this_ks.current_cages.remove(empty_cell_cage)

    empty_cell.set_solid_value(value)
    empty_cell.set_complete(True)
