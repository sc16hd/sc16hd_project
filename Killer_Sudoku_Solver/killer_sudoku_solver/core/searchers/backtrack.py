import os
import datetime


def clear():
    """Clears the console. Makes the search easier to visualise when running in the console."""

    os.system('clear')


def backtrack_solve(ks, start_time=None, timeout=None, show_progress=False):
    """Performs a backtrack iteration on the first empty cell in the puzzle."""

    if timeout:
        if (datetime.datetime.now() - start_time).total_seconds() * 1000 > timeout.total_seconds() * 1000:
            return "timed_out"

    empty_cell = None
    for cell in ks.get_cells():
        if not cell.get_complete():
            empty_cell = cell
            break

    if not empty_cell:
        return "solved"

    for value in empty_cell.get_pencil_values():

        empty_cell.set_solid_value(value)
        empty_cell.set_complete(True)

        if show_progress:
            clear()
            print()
            ks.print_solid_values()
            print()

        if check_if_valid(ks, empty_cell):
            status = backtrack_solve(ks=ks, start_time=start_time, timeout=timeout, show_progress=show_progress)
            if status is not False:
                return status

        empty_cell.set_solid_value(0)
        empty_cell.set_complete(False)

    return False


def check_if_valid(ks, check_cell):
    """Checks if the value assigned to check_cell is valid."""

    row_id = check_cell.get_row()
    cells_in_row = ks.find_cells_in_house("row", row_id)
    for cell in cells_in_row:
        if cell != check_cell:
            if cell.get_solid_value() == check_cell.get_solid_value():
                return False

    col_id = check_cell.get_col()
    cells_in_col = ks.find_cells_in_house("col", col_id)
    for cell in cells_in_col:
        if cell != check_cell:
            if cell.get_solid_value() == check_cell.get_solid_value():
                return False

    box_id = check_cell.get_box()
    cells_in_box = ks.find_cells_in_house("box", box_id)
    for cell in cells_in_box:
        if cell != check_cell:
            if cell.get_solid_value() == check_cell.get_solid_value():
                return False

    for cage in ks.get_current_cages():
        if check_cell in cage.get_cells():
            cage_cell_values = [cell.get_solid_value() for cell in cage.get_cells()]
            if 0 not in cage_cell_values:
                if sum(cage_cell_values) != cage.get_clue():
                    return False
            if cage_cell_values.count(check_cell.get_solid_value()) != 1:
                return False

    return True
