import os
import datetime


def clear():
    """Clears the console. Makes the search easier to visualise when running in the console."""

    os.system('clear')


def backjump_solve(ks, i, start_time=None, timeout=None, show_progress=False):
    """Performs a backjump iteration at level i."""

    if timeout:
        if (datetime.datetime.now() - start_time).total_seconds() * 1000 > timeout.total_seconds() * 1000:
            return "timed_out"

    return_depth = 0
    max_check_level = 0

    # i_cell is the cell whose domain is being iterated through
    i_cell = ks.get_cells()[i]

    # Try to find a consistent value in i_cell's domain
    for value in i_cell.get_pencil_values():

        consistent = True
        i_cell.set_solid_value(value)
        i_cell.set_complete(True)

        if show_progress:
            clear()
            print()
            ks.print_solid_values()
            print()

        # Check if each assigned value is consistent with other cells. For each assignment, record the last cell that
        # was consistent
        for h in range(0, len(ks.get_cells()[:i])):
            h_cell = ks.get_cells()[h]
            consistent = test(i_cell, h_cell)
            max_check_level = h
            if not consistent:
                break

        if consistent:

            # If the last cell in the puzzle has a consistent assignment, the puzzle is solved
            if i_cell == ks.get_cells()[-1]:
                return "solved"
            else:

                # Move on to the next cell
                status = backjump_solve(ks=ks, i=i+1, start_time=start_time, timeout=timeout,
                                        show_progress=show_progress)

                # If status is a number, the search has not finished, so continue
                if isinstance(status, int):
                    max_check_level = status

                    # If the returned max_check_level is lower than the current level, i, the i_cell must be set to 0
                    # and max_check_level returned to the i - 1 upper level
                    if max_check_level < i:
                        i_cell.set_solid_value(0)
                        i_cell.set_complete(False)
                        return max_check_level

                # Else status is either "solved" or "timed_out". In either case, return through all the levels so the
                # search ends
                else:
                    return status

        # The return depth is the highest level that had a consistent assignment when iterating through the domain of
        # i_cell
        return_depth = max(return_depth, max_check_level)

    # If execution reaches here then the domain of i_cell has been exhausted, and no consistent assignments exist
    i_cell.set_solid_value(0)
    i_cell.set_complete(False)
    return return_depth


def test(i_cell, h_cell):
    """Test if i_cell's assignment is consistent with j_cell's assignment"""

    cells_share_house = False
    cells_share_cage = False
    if i_cell.get_row() == h_cell.get_row():
        cells_share_house = True
    if i_cell.get_col() == h_cell.get_col():
        cells_share_house = True
    if i_cell.get_box() == h_cell.get_box():
        cells_share_house = True
    if i_cell.get_original_cage() == h_cell.get_original_cage():
        cells_share_cage = True

    # If cells do not share a house or cage, they must be consistent whatever their assigned values are
    if cells_share_house or cells_share_cage:

        if i_cell.get_solid_value() == h_cell.get_solid_value():
            return False

        if cells_share_cage:

            cage = i_cell.get_original_cage()

            # If the cage is full but it's clue is not satisfied, only the last two cells in the cage are deemed
            # inconsistent. If all cage cells are deemed inconsistent, the solution could be missed.
            if cage.cage_full():
                if h_cell == cage.get_cells()[-2]:
                    if cage.sum() != cage.get_clue():
                        return False

    return True
