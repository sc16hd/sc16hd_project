from setuptools import setup

setup(name='Killer_Sudoku_Solver',
      version='1.0',
      description='Solving tool for Killer Sudoku puzzles',
      author='Henry Davies',
      author_email='sc16hd@leeds.ac.uk',)
