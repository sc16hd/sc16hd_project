multi_config = {
    "output_option": "one_file",
    "ks_name": ["dks_2744"],
    "use_rule_solve": [True, False],
    "rule_solve_end_condition": ["none"],
    "rule_solve_min_rdv": [0],
    "rule_solve_timeout": [0],
    "rule_solve_max_blocks": [4],
    "rule_solve_max_cells": [4],
    "next_strategy": ["backjump", "minizinc"],
    "search_timeout": [10000],
    "cell_order_option": ["d"],
    "minizinc_gen_pseudos": [False],
    "minizinc_max_blocks": [1],
    "minizinc_max_cells": [1]
}
